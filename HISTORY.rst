0.2.1 - 2019-07-27
------------------

- Fix bug in ``YAMLSerializer`` to work with new files

0.2.0 - 2016-04-12
------------------

- Add ``YAMLSerializer`` and ``yaml11`` extra for installation

0.1.0 - 2015-06-21
------------------

- Add ``PrettyJSONSerializer``
