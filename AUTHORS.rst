Development Lead
----------------

- Ian Stapleton Cordasco  <graffatcolmingov@gmail.com>

Contributors
------------

YAMLSerializer
``````````````

- Jamie Lennox, original author

PrettyJSONSerializer
````````````````````

- Jeremy Thurgood, original author
